@extends('layouts.app')

@section('content')
{{-- {-- ________________________________TITRE_______________________________________________ --}} 
<div class="jumbotron jumbotron-fluid bg-white" id="title_rse">      
    <div class="title_titre">
      <h1 class="display-4 text-center">LA RSE </h1>
    </div>
</div>
{{-- _________________________Bloc Qu'est-ce que la RSE________________________________________ --}}
<div class="container-fluid text-center">
  <div class="  mx-auto"> 
    <h3 style="color:#949393">Qu'est-ce que la Responsabilité Sociale des Entreprises ?</h3><br><br>
    <div class="mx-auto"  id="bloc_rse_video">
      <div class="mx-auto" id="video_separer">
        {{-- LA GOUVERNANCE --}}
        <a href="https://www.youtube.com/watch?v=vu0deUSXbFk">LA GOUVERNANCE
          <div class="d-flex align-items-center justify-content-center" style="background-image:url(/img/caroussel_1.jpg);" id="ContainerVideo1">
            <ion-icon id="youtube" name="logo-youtube"></ion-icon>
          </div>
        </a>
        {{-- LE DIALOGUE  --}}
        <a href="https://www.youtube.com/watch?v=XQBMAOOTDPs">LE DIALOGUE AVEC LES PARTIES PRENANTES
          <div class="d-flex align-items-center justify-content-center" style="background-image:url(/img/caroussel_2.jpg);" id="ContainerVideo1">
            <ion-icon id="youtube" name="logo-youtube"></ion-icon>
          </div>
        </a>
        {{-- ANCRAGE TERRITORIAL  --}}
        <a href="https://www.youtube.com/watch?v=Msc7JidYGmA">ANCRAGE TERRITORIAL
          <div class="d-flex align-items-center justify-content-center" style="background-image:url(/img/caroussel_3.jpg);" id="ContainerVideo1">
            <ion-icon id="youtube" name="logo-youtube"></ion-icon>
          </div>
        </a>
      </div><br>
      <div class="mx-auto" id="video_separer">
        {{-- LE DIALOGUE SOCIAL  --}}
        <a href="https://www.youtube.com/watch?v=UH9YgPV4zxc">LE DIALOGUE SOCIAL
          <div class="d-flex align-items-center justify-content-center" style="background-image:url(/img/caroussel_5.jpg);" id="ContainerVideo1">
            <ion-icon id="youtube" name="logo-youtube"></ion-icon>
          </div>
        </a>
        {{-- SANTÉ ET SECURITÉ, ENJEUX ET INTERETS DE LA PRÉVENTION  --}}
        <a class="text_video" href="https://www.youtube.com/watch?v=5plURFBmLNo">SANTÉ ET SECURITÉ, ENJEUX ET INTÉRÊTS DE LA PRÉVENTION
          <div class="d-flex align-items-center justify-content-center" style="background-image:url(/img/caroussel_6.jpg);" id="ContainerVideo1">
            <ion-icon id="youtube" name="logo-youtube"></ion-icon>
          </div>
        </a>
      </div>
    </div>
  </div><br><br>
</div>
<!-- ________________________________________Bloc Pourquoi s'engager_________________________________________________________  -->
<section class="" id="block-why">
  <div class="container-fluid text-center bg-light" ><br><br>
    <div class=" w-75 p-3 col-md-6 mx-auto"> 
      <h3 style="color:#588623" >Pourquoi s'engager ?</h3><br><br>
      <p class="text-justify"> Les SCOP du BTP sont porteuses au sein de la société française d’une éthique économique et sociale forte,
          qui les rend particulièrement sensibles au développement durable par les valeurs de partage, de respect,
          d’entraide et de solidarité qu’elles mettent en avant.
          La politique engagée en matière de développement durable depuis 2008 puis en matière de Responsabilité
          Sociétale de l’Entreprise (RSE) depuis 2011 par la Fédération des SCOP du BTP vise à inscrire les SCOP du BTP
          dans la pérennité et le développement de l’activité et de l’emploi, tout autant que dans l’accompagnement des
          mutations sociétales et environnementales du XXIème siècle.</p>
    </div><br><br>
  </div><br><br>
  <!-- ________________________________________RAISON DE S'engager_________________________________________________________  -->
  <div class="container-fluid text-center " ><br><br>
    <h3 style="color:#588623">Vous voulez plus de raisons de vous engager ? C’est parti !</h3><br>
    <div id="sengager">
      <div class="mx-auto" id="bloc_paragraphe"> 
        <ul class=" "><br>
          <li class=""><ion-icon name="arrow-round-forward"></ion-icon> 576 collaborateurs sensibilisés à la RSE </li><br>
          <li class=""><ion-icon name="arrow-round-forward"></ion-icon>10 SCOP du BTP engagées dans une démarche RSE </li><br>
          <li class=""><ion-icon name="arrow-round-forward"></ion-icon> Déchets divisés par 5 en 4 ans</li><br>
          <li class=""><ion-icon name="arrow-round-forward"></ion-icon> 95% des salariés en CDI  </li><br> 
        </ul>   
        <ul class=" "><br>
          <li class=""><ion-icon name="arrow-round-forward"></ion-icon> 47% de femmes en formation </li><br>
          <li class=""><ion-icon name="arrow-round-forward"></ion-icon> 100% d’investissement sur le territoire </li> <br>
          <li class=""><ion-icon name="arrow-round-forward"></ion-icon> Plus de 3% de la masse salariale dédiée à la formation </li><br>
          <li class=""><ion-icon name="arrow-round-forward"></ion-icon> Un accompagnement à travers un Club RSE et des groupes de travail trimestriels </li><br>   
        </ul>           
      </div>
    </div>
  </div>
  {{-- <hr style="border:1px solid lightgrey; width:70%"> --}}
  <br><br>
  <div class="container-fluid text-center  border border-success w-50" ><br>
    <p>Vous n’êtes pas encore engagés mais la démarche RSE vous intéresse ? <br>
      La fédération SO SCOP BTP vous accompagne ! 
        N’hésitez pas à nous rejoindre lors de notre prochain Atelier du Club RSE.</p>
     <button type="button"class="btn btn-outline-success"><a  href="/agenda" >voir l'Agenda</a></button><br><br>
  </div>
</section><br><br>
<!--____________________________________________ Bloc engagés__________________________________________________________________________  -->
<section class="container-fluid text-center bg-light " id="block-engaged"><br><br>
  <h3 style="color:#588623">S'engager, c'est bénéficier : </h3><br>
  <div class="bloc_engager mx-auto">
    <img src="img/engager.jpg" alt="" width="500px" height="300px">
    <div class="text_engager">
      <ul> 
        <li><ion-icon name="arrow-round-forward"></ion-icon>D'un accompagnement personnalisé de la Fédération SO SCOP BTP</li> 
        <li><ion-icon name="arrow-round-forward"></ion-icon>D'un label reconnu
        <li><ion-icon name="arrow-round-forward"></ion-icon>D'un accès au Club RSE (Groupes de travail et/ou ateliers) lien calendrier</li>
        <li><ion-icon name="arrow-round-forward"></ion-icon>De rencontres de partenaires, professionnels...</li>
      </ul>
    </div><br><br>
  </div> <br><br>
</section>
<section><br><br>
  <div class="container-fluid mx-auto text-center"><br><br>
      <h3 style="color:#588623">Comment s'engager ?</h3>
    <img src="img/RSE-COMMENT.JPG" width="80%" height="80%"class="mx-auto" alt="">
  </div><br>
</section><br>
<hr style="border:1px solid lightgrey; width:70%"><br>
{{-- _________________list scops engagées_____________________________ --}}
<section class="container-fluid w-100  text-center" id="block-engaged">
  <h3 class="text-center mx-auto " style="color:#588623">Qui sont les SCOP du BTP en Occitanie déjà engagées dans la RSE ?</h3><br><br>
  <div class="mx-auto" id="list-label">
    <div class="cart_label_labeliser">
      <h6>CITEL<h6>
      <a href="https://scop-citel.fr/"><ion-icon name="globe"></ion-icon></a><br>
      <span class="badge badge-warning">Labellisé !</span>
    </div>
    <div class="cart_label_labeliser">
      <h6>Courserans Construction</h6>
      <a href="http://couseransconstruction.fr/"><ion-icon name="globe"></ion-icon></a>
      <a href="https://www.facebook.com/couseransconstructionscop/"><ion-icon name="logo-facebook"></ion-icon></a>
      <a href="https://www.instagram.com/couseransconstructionscop9/"><ion-icon name="logo-instagram"></ion-icon></a><br>
      <span class="badge badge-warning">Labellisé !</span>
    </div>
    <div class="cart_label_labeliser">
      <h6>Regabat Coopérative</h6>
      <a href="https://www.regate.fr/"><ion-icon name="globe"></ion-icon></a>
      <a href="https://www.facebook.com/CaeRegateRegabat/"><ion-icon name="logo-facebook"></ion-icon></a><br>
      <span class="badge badge-warning">Labellisé !</span>
    </div>
    {{-- <hr style="border:1px solid lightgrey; width:70%"> --}}
    <div class="scoop_non_labelise mx-auto">   
      <div class="cart_label">
        <h6>AHJ</h6> 
        <a href="https://www.ahj-tarn.fr/"><ion-icon name="globe"></ion-icon></a>
        <a href="https://www.facebook.com/AHJ-scop-1558661697797526/"><ion-icon name="logo-facebook"></ion-icon></a><br>
        <span class="badge badge-secondary">Engagé !</span>
      </div>
      <div class="cart_label">
        <h6>BV Scoop</h6>
        <a href="http://www.bvscop.com"><ion-icon name="globe"></ion-icon>bvscop.com</a><br>
        <span class="badge badge-secondary">Engagé !</span>
      </div>
      <div class="cart_label">
        <h6>Cabrol</h6>
        <a href="http://cabrol.fr/"><ion-icon name="globe"></ion-icon></a>
        <a href="https://www.facebook.com/CabrolSCOP/"><ion-icon name="logo-facebook"></ion-icon></a>
        <a href="https://www.linkedin.com/company/scop-cabrol/"><ion-icon name="logo-linkedin"></ion-icon></a><br>
        <span class="badge badge-secondary">Engagé !</span>
      </div>
      <div class="cart_label">
        <h6>EGA</h6>
        <a href="https://www.facebook.com/Electricit%C3%A9-G%C3%A9n%C3%A9rale-Ari%C3%A9geoise-764661100286712/"><ion-icon name="logo-facebook">Page Facebook</ion-icon></a><br> 
        <span class="badge badge-secondary">Engagé !</span> 
      </div>       
      <div class="cart_label">
        <h6>Tournée du Coq</h6>
        <a href="https://latourneeducoq.com/"><ion-icon name="globe"></ion-icon><span class="label_text">latourneeducoq.com</span></a><br>
        <span class="badge badge-secondary">Engagé !</span>
      </div>
    </div>
  </div><br><br>
</section>
{{-- ________Paragraphe Label_____ --}}
<section id="LABEL">
  <div class="container-fluid mx-auto  text-center"><br>
    <h3 style="color:#588623">Le Label RSE SCOP BTP</h3>
    <h4 style="color:#949393">Un Label pionnier et en phase de reconnaissance par les pouvoirs public</h4>
    <div class="w-75 p-3  mx-auto"><br>
      <p class="text-justify">Un Label est une marque de reconnaissance par une tierce
          partie d’un dispositif mis en œuvre par un organisme sur un thème précis, par rapport 
          aux dispositions d’un cahier des charges et ce au moyen, notamment, d’évaluations
          récurrentes sur site ou non.
          Le Label RSE Scop BTP sera la marque de reconnaissance couronnant une évaluation positive 
          d’une coopérative BTP au modèle AFAQ 26000. Ce Label sera délivré par un comité de labélisation 
          composé de parties prenantes représentatives de toute la branche BTP : maitres d’ouvrage publics
          et privés, banque, assurances ; organismes de prévention, protection sociale.</p>
    </div>
  </div><br><br>
</section><br><br>
@endsection