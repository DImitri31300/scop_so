@extends('layouts.app')

@section('content')
{{-- ________________________________TITRE_______________________________________________ --}}
<div class="jumbotron jumbotron-fluid bg-white" id="title_agenda">      
    <div class="title_titre">
      <h1 class="display-4 text-center">AGENDA</h1>
    </div>
</div>
<section class="w-75 mx-auto">


<div class="flex-center  full-height">

    <!-- <p>Adhérents, connectez-vous pour visualiser vos événements réservés</p>
        @if (Route::has('login'))
        <div class="top-right links">
            @auth
            <a href="{{ url('/agenda') }}">Agenda</a>
            @else
            <a href="{{ route('login') }}">Login</a>

            @if (Route::has('register'))
            <a href="{{ route('register') }}">Register</a>
            @endif
            @endauth
        </div>
        @endif -->


   
    @foreach($events as $event)
    <div class="card-deck">
        <div class="card bg-light mb-3" style="max-width: 18rem;">
            <div class="card-header text-success">{{ (new DateTime($event->date_event))->format('d F Y') }}</div>
            <div class="card-body">
                <h5 class="card-title">{{ $event->title }}</h5>
                <p class="card-text">{{ $event->content }}</p>
                <div class="share-post">
                    <div class="label text-center">Partager</div>
                    <div class="space_rs text-center">
                    {{-- <ul class=""> --}}
                        <li class="twitter"><ion-icon name="logo-twitter"></ion-icon>
                            <a class="share" target="_blank" data-width="700" data-height="400" title="Twitter" href="" rel="nofollow">
                                <i class="fa fa-twitter" aria-hidden="true">
                                </i>
                            </a>
                        </li>
                        <li class="facebook"><ion-icon name="logo-facebook"></ion-icon>
                            <a class="share" target="_blank" data-width="700" data-height="400" title="Facebook" href="" rel="nofollow">
                                <i class="fa fa-facebook" aria-hidden="true">
                                </i>
                            </a>
                        </li>
                        <li class="linkedin"><ion-icon name="logo-linkedin"></ion-icon>
                            <a class="share" target="_blank" data-width="700" data-height="400" title="Linkedin" href="" rel="nofollow">
                                <i class="fa fa-linkedin" aria-hidden="true">
                                </i>
                            </a>
                        </li>
                    {{-- </ul> --}}
                </div><br>
                </div> 
                <p class="card-text"><span class="font-weight-bold">Lieu :</span> {{ $event->place }}</p>
                <p class="card-text"><span class="font-weight-bold">Contact : </span>{{ $event->contact }}</p>
            </div>
        </div>
    </div>
</div>    
@endforeach
</div>
</section>
    @endsection