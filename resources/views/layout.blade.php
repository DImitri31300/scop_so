{{-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    {{-- <link rel="stylesheet" href="/css/bulma-0.8.0/css/bulma.css"> --}}
    {{-- <link rel="stylesheet" href="/css/app.css"> --}}
     <!-- Styles -->
    {{-- <link href="css/app.css" rel="stylesheet">
    
    <title>SCOP BTP</title>
</head>
<body>
    <header>
      <nav class="navbar navbar-expand-lg navbar-light "style="background-color: white;">
        <a class="navbar-brand" href="/home">
          <img src="/img/logo.jpg" width="200" height="200" alt="retour à l'accueil">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto ">
            <li class="nav-item active">
              <a class="nav-link" href="/">Accueil <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/federation">La Fédération</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/rse">La RSE</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">L'Actualités</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">L'Annuaire</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">L'Agenda</a>
            </li>
          </ul>
          <form class="form-inline my-2 my-lg-0">
                  <a class="nav-link btn btn-outline-success my-2 my-sm-0" href="/login">ESPACE ADHÉRENT</a>
          </form>
        </div>
      </nav>

  
    </header>

    @yield('contenu')

    <!-- Footer -->
<footer class="page-footer  font-small  bg-light pt-4 ">
  <div class="container-fluid text-center text-md-left">
    <div class="row">
      <div class="col-md-6 mt-md-0 mt-3">
        <!-- Content -->
        <h5 class="text-uppercase">Fédération SCOP BTP</h5>
        <img src="/img/logo.jpg" width="200" height="200" alt="">
      </div>
      <!-- Grid column -->
      <hr class="clearfix w-100 d-md-none pb-3">
      <div class="col-md-3 mb-md-0 mb-3">
        <!-- menu -->
        <h5 class="text-uppercase">Menu</h5>

        <ul class="list-unstyled">
          <li>
            <a href="#!">Accueil</a>
          </li>
          <li>
            <a href="#!">La fédération</a>
          </li>
          <li>
            <a href="#!">La RSE</a>
          </li>
          <li>
            <a href="#!">Actualitéz</a>
          </li>
          <li>
            <a href="#!">Annuaire</a>
          </li>
          <li>
            <a href="#!">Agenda</a>
          </li>
          <li>
            <a href="#!">Esapce adhérent</a>
          </li>
        </ul>
      </div>
      <div class="col-md-3 mb-md-0 mb-3">
        <h5 class="text-uppercase">Contact</h5>

        <ul class="list-unstyled">
          <li>
            <p><strong>Adresse :</strong></p>
            <p>Parc du Canal, Napa Center
              Bâtiment A,
              3 rue Ariane
              31520 Ramonville Saint-Agne</p>
          </li>
          <li>
            <p><strong>Téléphone :</strong></p>
            <p>05 61 00 20 18</p>
          </li>
          <li>
              <p><strong>Email :</strong></p>
            <p>sudouest@scopbtp.org</p>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© 2019 Copyright:
    <a href=""> Fédération SCOP BTP SO</a>
  </div>
</footer>
<script src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons.js"></script>
</body>

</html> --}} 