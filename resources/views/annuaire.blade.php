@extends('layouts.app')


@section('content')
{{-- ________________________________TITRE_______________________________________________ --}}
<div class="jumbotron jumbotron-fluid bg-white" id="title_annuaire">      
    <div class="title_titre">
        <h1 class="display-4 text-center"> Annuaire des SCOPS adhérentes</h1>
    </div>
</div>
{{-- ___________________________CARTE SCOP_________________ --}}
<section class="w-75 mx-auto">
    <div class="bloc_annuaire w-75 mx-auto">
        <div class="">
            <div class="container " id="container_annuaire">
                <div class="row">
                    <div class="box">
                        @foreach($scops as $scop)
	                        <div class="our-team-main">
	                            <div class="team-front">
	                                <img src="" class="img-fluid" />
	                                <h3 class="text-success">{{ $scop->Nom_scop }}</h3>
	                                <p>Activité(s) de la Scop : {{ $scop->Activites }}</p>
	                            </div>
	                            <div class="team-back">
                                    <p><span class="font-weight-bold" >Département :</span> {{ $scop->Departement }}</p>
                                    <p><span class="font-weight-bold" >Mail :</span>  {{ $scop->Mail }}</p>
                                    <p><span class="font-weight-bold" >Téléphone :</span>  {{ $scop->Telephone }}</p>
                                    <p><span class="font-weight-bold" >Site Internet :</span>  {{ $scop->Site_internet }}</p>
                                    <p><span class="font-weight-bold" >Engagée dans une démarche RSE : </span> {{ $scop->Engagee_RSE }}</p>
                                    <p><span class="font-weight-bold" >Labellisée RSE :</span>  {{ $scop->Labellisee_RSE }}</p>
	                            </div>
	                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><br><br>
@endsection
