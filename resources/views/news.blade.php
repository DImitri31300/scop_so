@extends('layouts.app')


@section('content')
{{-- ________________________________TITRE_______________________________________________ --}}
<div class="jumbotron jumbotron-fluid bg-white" id="title_new">
    <div class="title_titre">
        <h1 class="display-4 text-center">ACTUALITÉ </h1>
    </div>
</div>
{{-- {-- _________________________________ARTICLES__________________________________________ --}}
<div class="container-fluid mx-auto text-center">
    <br>
    <h3 style="color:#949393">Retrouvez toute l'actualité de la Fédération et des SCOPS adhérentes</h3><br>
    {{-- _______________________________DERNIERS ARTICLES____________________________________ --}}

    <section class="list_article">
        @foreach($news as $new)
        <div id="ACTU">
            <div class="card" style="width: 18rem;">
                <img src="{{ $new->image }}" class="card-img-top" alt="image représentant l'évènement">
                <div class="card-body">
                    <h5 class="card-title">{{ $new->title }}</h5>
                    <p class="card-text">{{ $new->excerpt }}</p>
                    <button type="button" class="btn btn-outline-success">Lire la suite</button>
                </div>
            </div>
        </div><br>
        @endforeach
    </section>
</div><br><br>
{{-- ______________________________________RESEAUX SOCIAUX____________________________________________ --}}
<section class="RS" id="ContainerRS">
    <div class="container-fluid text-center bg-light">
        <div class=" w-75  mx-auto">
            <div class="bloc_RSActu"><br>
                <h4>Suivez toute notre actualité sur les réseaux sociaux</h4><br>
                <div class="rs">
                    <a href="https://twitter.com/so_btp">
                        <ion-icon class="logo_rs" name="logo-twitter"></ion-icon>
                    </a>
                    <a href="https://www.linkedin.com/company/f%C3%A9d%C3%A9ration-sud-ouest-scop-btp/?viewAsMember=true">
                        <ion-icon class="logo_rs" name="logo-linkedin"></ion-icon>
                    </a>
                    <a href="https://www.youtube.com/user/SCOPBTP">
                        <ion-icon class="logo_rs" name="logo-youtube"></ion-icon>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection