@extends('layouts.app')

@section('content')

<main id="federation">
 {{-- ________________________________TITRE_______________________________________________ --}}
<div class="jumbotron jumbotron-fluid bg-white" id="title_federation">      
  <div class="title_titre">
    <h1 class="display-4 text-center">LA FÉDÉRATION </h1>
  </div>
</div>
{{-- _____________________SECTION : qui sommes-nous?_______________________________________ --}}
<div id="qui_somme_nous" class="container-fluid text-center"><br>
  <h2 class="" style="color:#588623" >Qui sommes-nous ?</h2><br>
    <div class=" w-50 p-3 col-md-6 mx-auto"> 
      <h4 style="color:#949393">La Fédération des SCOP du BTP :</h4><br>
      <p class="text-justify">Créée en 1946, la Fédération des SCOP du BTP, est un syndicat professionnel reconnu par
          les pouvoirs publics. Fédération professionnelle représentant le premier réseau de PME
          indépendantes et participatives du Bâtiment et des Travaux Publics. Ses missions sont
          centrées sur la défense des intérêts et des spécificités des SCOP BTP et le conseil.</p>
    </div>
    <p>La Fédération des SCOP du BTP a pour missions essentielles :</p>
</div>      
{{-- ___________________________BLOC DES 4 paragraphes____________________________________________ --}}        
<div class="paragraphe_fede">
  <div class="offset-md-2" id="bloc_paragraphe"> 
    <ul class="w-75 bg-light"><br>
      <li class=""><ion-icon name="arrow-round-forward"></ion-icon> 
          De représenter et de défendre les intérêts des SCOP BTP auprès des pouvoirs
          publics et politiques ainsi qu’au niveau européen pour influencer toutes décisions qui
          ont une incidence sur le marché et le fonctionnement des SCOP du BTP et les
          conditions d’exercice de la profession de la Construction,
      </li><br>
      <li class=""><ion-icon name="arrow-round-forward"></ion-icon> 
          De promouvoir et valoriser l’image des SCOP BTP en développant une
            communication dynamique à destination de son environnement économique, social et
          D’apporter son expertise juridique, sociale, économique
      </li><br>
      <li class=""><ion-icon name="arrow-round-forward"></ion-icon>  
          D’assurer un service de proximité auprès des SCOP BTP de – 11 salariés en
          matière de formation des Coopérateurs et d’ingénierie de la formation
      </li><br>
      <li class=""><ion-icon name="arrow-round-forward"></ion-icon> 
          De faciliter l’inter-coopération entre les SCOP BTP, pour promouvoir les échanges
          et la mutualisation des expériences et des bonnes pratiques et d’apporter son expertise juridique, sociale, économique et technique aux SCOP
          BTP pour les aider et les conseiller
      </li><br>   
    </ul>           
  </div>
</div> 
<br><br>
<hr style="border:1px solid lightgrey; width:70%"><br>
{{-- ________________________SECTION _CHIFRE SUR LA FEDE_________________________ --}}
<div class="container" >
  <div class="row align-items-top mx-auto" id="box_chiffre">
    <div class="col text-center" >
      <p class="display-3 " style="color:#588623">10</p><br>
      <p> En France, on compte 10 Fédérations régionales des SCOP du BTP réparties sur tout le
           territoire et présentes dans chaque régions. </p>
    </div>
    <div class="col text-center">
      <p class="display-3 " style="color:#588623">419</p><br>
      <p >En 2019, on recensait 414 SCOP adhérentes à la Fédération des SCOP 
          du BTP en France et 9683 salariés.</p>
    </div>
    <div class="col text-center">
      <p class="display-3 " style="color:#588623">48</p><br>
      <p> En Occitanie, nous accompagnons 48 SCOP du BTP dont une partie d’entre elles (16%) est
          engagée et conseillée dans une démarche RSE. –lien vers l’annuaire </p>
    </div>
  </div>
</div><br>
<hr style="border:1px solid lightgrey; width:70%">
{{-- ____________________SECTION SCOP : LE STATUS DES SCOPS_______________________________ --}}
<br><br>
<section class="text-center w-100 d-flex flex-column">
  <h3 class="display-4 text-center mx-auto" style="color:#588623" >LE STATUT SCOP</h3><br>
  <div id="statut" class="w-75 mx-auto text-center " >
    <div class="vidos">
      <iframe width="600" height="350" src="https://www.youtube.com/embed/TTSmRoWNozY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
    <div class="w-50 p-3 d-flex align-items-center " >
      <p class="mb-0 text-justify">Les Sociétés coopératives et participatives sont des sociétés de forme SA, SARL ou SAS,
        dont les salariés sont associés majoritaires et vivent un projet commun en mutualisant
        équitablement les risques et les grandes décisions stratégiques.
        Parmi les structures coopératives, les SCOP sont les seules dont les membres associés sont
        les salariés.
        Les Scop reposent sur un principe de démocratie d’entreprise et de priorité à la pérennité du
        projet.</p>
    </div>
  </div>
</section><br><br><br><br>
{{-- _________________________2parti : FLECHE --}}
<div class="fleche mx-auto">
{{-- FLECHE 1 --}}
  <div class="wrapper">
    <div class="explain_arrow">
      <p class="display-4">1</p> 
    </div>
    <div class="text_arrow">Des entreprises concurrentielles</div>
    <div class="triangle right"></div>
  </div>
{{-- FLECHE 2 --}}
  <div class="wrapper">
    <div class="explain_arrow">
      <p class="display-4">2</p> 
    </div>
    <div class="text_arrow">Une gouvernance démocratique</div>
    <div class="triangle right"></div>
  </div>      
{{-- FLECHE 3 --}}
  <div class="wrapper">
    <div class="explain_arrow">
      <p class="display-4">3</p> 
    </div>
    <div class="text_arrow">Une répartition équitable des bénéfices</div>
    <div class="triangle right"></div>      
  </div> 
{{-- FLECHE 4 --}}
  <div class="wrapper"
    <div class="explain_arrow">
      <p class="display-4">4</p> 
    </div>
    <div class="text_arrow">Une priorité à la pérennité de l’entreprise</div>
  </div> 
</div> 
<hr style="border:1px solid lightgrey; width:90%"><br>
<div class="fleche_number_text offset-md-1"> 
  {{-- text fleche 1 --}}
  <div class="wrapper">
      <div class="explain_arrow">
          <p>Double qualité d'associé et de salarié</p>
      </div>
  </div>
  {{-- text fleche 2 --}}
  <div class="wrapper">
      <div class="explain_arrow">
          <p>1 personne = 1 voix</p>
      </div>
  </div> 
  {{-- texte fleche 3 --}}
  <div class="wrapper">
      <div class="explain_arrow">
          <p>Salariée, entreprise, associés, 
              tous perçoivent une part des bénéfices</p>
      </div>
  </div> 
  {{-- texte fleche 4 --}}
  <div class="wrapper">
      <div class="explain_arrow">
          <p>Réserves impartageables</p>
      </div>
  </div>     
</div>
{{-- _______________NOS PARTENAIRES___________________ --}}
<section id="partenaire">
  <div id="bloc_partenaire" class="w-75 p-3 mx-auto" >
    <h3 class="display-4 text-center" style="color:#588623" >NOS PARTENAIRES</h3><br><br>
      <div id="bloc_logo_partenaire" class="d-flex flex-row">
        <div id="slider"> 
          <figure>
            <a href="https://www.credit-cooperatif.coop/Institutionnel" target="_blank">
              <img src="/img/LOGOS_PARTENAIRES/Crédit_coopératif.png" alt="logo crédit coopératif">
            </a>
            <a href="http://www.cibtp-sud-ouest.fr/"target="_blank">
              <img src="/img/LOGOS_PARTENAIRES/caisse_conge_paye.png" alt="logo CCP : Caisse des congés payés ">
            </a>
            <a href="https://www.preventionbtp.fr/" target="_blank">
              <img src="/img/LOGOS_PARTENAIRES/OPP_BTP.png" alt="logo OPP BTP">
            </a>
            <a href="https://www.constructys.fr/" target="_blank">
              <img src="/img/LOGOS_PARTENAIRES/constructys.png" alt="logo Constructys">
            </a>
            <a href="https://www.ccca-btp.fr/?gclid=Cj0KCQiAoIPvBRDgARIsAHsCw0-wEpsz3Zp1hVGOU1stBkkM-5bu632XBiA-MWeVJkkvH57aUhkC7xkaAl6uEALw_wcB" target="_blank">
              <img src="/img/LOGOS_PARTENAIRES/CCCA_BTP.png" alt="logo CCCA-BTP">
            </a>
            <a href="https://www.qualibat.com/" target="_blank">
              <img src="/img/LOGOS_PARTENAIRES/Qualibat.png" alt="logo Qualibat">
            </a>
            <a href="https://www.certibat.fr/" target="_blank">
              <img src="/img/LOGOS_PARTENAIRES/Certibat.png" alt="logo Certibat">
            </a>
            <a href="https://www.qualifelec.fr/" target="_blank">
              <img src="/img/LOGOS_PARTENAIRES/Qualifelec.png" alt="logo Qualifelec">
            </a>
            <a href="https://www.les-scop.coop/sites/fr/le-reseau/organisation/cg-scop" target="_blank">
              <img src="/img/LOGOS_PARTENAIRES/les_scop.png" alt="logo CG SCOP">
            </a>
            <a href="https://www.union-sociale.coop/app.php/" target="_blank">
              <img src="/img/LOGOS_PARTENAIRES/Union_sociale.png" alt="logo Union sociale des SCOP">
            </a>
            <a href="https://www.metiers-btp.fr/" target="_blank">
              <img src="/img/LOGOS_PARTENAIRES/Observatoire_des_metiers.png" alt="logo Observatoire des métiers du BTP">
            </a>
            <a href="https://www.opqibi.com/" target="_blank">
              <img src="/img/LOGOS_PARTENAIRES/Opqibi.png" alt="logo OPQIBI">
            </a>
            <a href="http://www.consuel.com/" target="_blank">
              <img src="/img/LOGOS_PARTENAIRES/Consuel.png" alt="logo Consuel">
            </a>
            <a href="https://cecop.coop/?lang=fr" target="_blank">
              <img src="/img/LOGOS_PARTENAIRES/Cecop.png" alt="logo CECOP- CICOPA">
            </a>
            <a href="https://www.afnor.org/" target="_blank">
              <img src="/img/LOGOS_PARTENAIRES/afnor.png" alt="logo AFNOR">
            </a>
            <a href="https://www.btp-banque.fr/entreprises" target="_blank">
              <img src="/img/LOGOS_PARTENAIRES/BTP_banque.png" alt="logo BTP BANQUE ">
            </a>
            <a href="https://www.groupe-sma.fr/" target="_blank">
              <img src="/img/LOGOS_PARTENAIRES/SMA.png" alt="logo SMA ">
            </a>
            <a href="https://www.probtp.com/particuliers-accueil.html" target="_blank">
              <img src="/img/LOGOS_PARTENAIRES/PRO_BTP.png" alt="logo PRO BTP ">
            </a>
            <a href="https://www.compagnons-du-devoir.com/" target="_blank">
              <img src="/img/LOGOS_PARTENAIRES/Compagnons_du_devoir.png" alt="logo Les compagnons du devoir">
            </a>
            <a href="https://www.compagnons-du-devoir.com/" target="_blank">
              <img src="/img/LOGOS_PARTENAIRES/direccte.png" width="150px" height="200px" alt="logo Direcctes">
            </a>
            <a href="https://www.carsat-mp.fr/home.html" target="_blank">
              <img src="/img/LOGOS_PARTENAIRES/carsat_logo.png" alt="logo CARSAT">
            </a>
            <a href="https://www.occitanie.cci.fr/" target="_blank">
              <img src="/img/LOGOS_PARTENAIRES/CCI_OCCITANIE.png" alt="logo CCI">
            </a>
            <a href="https://www.ademe.fr/" target="_blank">
              <img src="/img/LOGOS_PARTENAIRES/ADEME.png" alt="logo ADEME">
            </a>
            <a href="https://www.anact.fr/le-reseau-anact-aract-propose-un-outil-pour-aider-les-entreprises-initier-et-piloter-leur-demarche-0" target="_blank">
              <img src="/img/LOGOS_PARTENAIRES/ARACT.png" alt="logo ARACT/ANACT">
            </a>
          </figure>
        </div>
      </div>
    </div>
  </section>
<br><br><br>
@endsection