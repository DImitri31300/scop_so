<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
            $this->middleware('auth')->except('upload');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function upload(Request $request)
    {
        $paths = [];
        $files = $request->file('files');
    
        foreach ($files as $file)
        {
            // Generate a file name with extension
            $fileName = 'profile-'.time().'.'.$file->getClientOriginalExtension();
            // Save the file
            $paths[] = $file->storeAs('public/files', $fileName);
        }
        return back();
    }
}
