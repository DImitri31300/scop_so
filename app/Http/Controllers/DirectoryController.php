<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class DirectoryController extends Controller
{
    public function index()
    {
        $scops = DB::table('scops')->get();
        return view('annuaire', [
            'scops' => $scops ]);
    }
}
