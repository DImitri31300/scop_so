-- MySQL dump 10.16  Distrib 10.1.41-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: scopbtpso
-- ------------------------------------------------------
-- Server version	10.1.41-MariaDB-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_unique` (`slug`),
  KEY `categories_parent_id_foreign` (`parent_id`),
  CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,NULL,1,'Category 1','category-1','2019-12-04 16:18:01','2019-12-04 16:18:01'),(2,NULL,1,'Category 2','category-2','2019-12-04 16:18:01','2019-12-04 16:18:01');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_rows`
--

DROP TABLE IF EXISTS `data_rows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_rows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_rows`
--

LOCK TABLES `data_rows` WRITE;
/*!40000 ALTER TABLE `data_rows` DISABLE KEYS */;
INSERT INTO `data_rows` VALUES (1,1,'id','number','ID',1,0,0,0,0,0,NULL,1),(2,1,'name','text','Nom',1,1,1,1,1,1,NULL,2),(3,1,'email','text','Email',1,1,1,1,1,1,NULL,3),(4,1,'password','password','Mot de passe',1,0,0,1,1,0,NULL,4),(5,1,'remember_token','text','Token de rappel',0,0,0,0,0,0,NULL,5),(6,1,'created_at','timestamp','Créé le',0,1,1,0,0,0,NULL,6),(7,1,'updated_at','timestamp','Mis à jour le',0,0,0,0,0,0,NULL,7),(8,1,'avatar','image','Avatar',0,1,1,1,1,1,NULL,8),(9,1,'user_belongsto_role_relationship','relationship','Rôle',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}',10),(10,1,'user_belongstomany_role_relationship','relationship','Roles',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}',11),(11,1,'settings','hidden','Settings',0,0,0,0,0,0,NULL,12),(12,2,'id','number','ID',1,0,0,0,0,0,NULL,1),(13,2,'name','text','Nom',1,1,1,1,1,1,NULL,2),(14,2,'created_at','timestamp','Créé le',0,0,0,0,0,0,NULL,3),(15,2,'updated_at','timestamp','Mis à jour le',0,0,0,0,0,0,NULL,4),(16,3,'id','number','ID',1,0,0,0,0,0,NULL,1),(17,3,'name','text','Nom',1,1,1,1,1,1,NULL,2),(18,3,'created_at','timestamp','Créé le',0,0,0,0,0,0,NULL,3),(19,3,'updated_at','timestamp','Mis à jour le',0,0,0,0,0,0,NULL,4),(20,3,'display_name','text','Nom d\'affichage',1,1,1,1,1,1,NULL,5),(21,1,'role_id','text','Rôle',1,1,1,1,1,1,NULL,9),(22,4,'id','number','ID',1,0,0,0,0,0,NULL,1),(23,4,'parent_id','select_dropdown','Parent',0,0,1,1,1,1,'{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}',2),(24,4,'order','text','Ordre',1,1,1,1,1,1,'{\"default\":1}',3),(25,4,'name','text','Nom',1,1,1,1,1,1,NULL,4),(26,4,'slug','text','Slug',1,1,1,1,1,1,'{\"slugify\":{\"origin\":\"name\"}}',5),(27,4,'created_at','timestamp','Créé le',0,0,1,0,0,0,NULL,6),(28,4,'updated_at','timestamp','Mis à jour le',0,0,0,0,0,0,NULL,7),(29,5,'id','number','ID',1,0,0,0,0,0,NULL,1),(30,5,'author_id','text','Auteur',1,0,1,1,0,1,NULL,2),(31,5,'category_id','text','Catégorie',1,0,1,1,1,0,NULL,3),(32,5,'title','text','Titre',1,1,1,1,1,1,NULL,4),(33,5,'excerpt','text_area','Extrait',1,0,1,1,1,1,NULL,5),(34,5,'body','rich_text_box','Corps',1,0,1,1,1,1,NULL,6),(35,5,'image','image','Image de l\'article',0,1,1,1,1,1,'{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}',7),(36,5,'slug','text','Slug',1,0,1,1,1,1,'{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}',8),(37,5,'meta_description','text_area','Meta Description',1,0,1,1,1,1,NULL,9),(38,5,'meta_keywords','text_area','Meta Mots-clés',1,0,1,1,1,1,NULL,10),(39,5,'status','select_dropdown','Statut',1,1,1,1,1,1,'{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}',11),(40,5,'created_at','timestamp','Créé le',0,1,1,0,0,0,NULL,12),(41,5,'updated_at','timestamp','Mis à jour le',0,0,0,0,0,0,NULL,13),(42,5,'seo_title','text','Titre SEO',0,1,1,1,1,1,NULL,14),(43,5,'featured','checkbox','Mis en avant',1,1,1,1,1,1,NULL,15),(44,6,'id','number','ID',1,0,0,0,0,0,NULL,1),(45,6,'author_id','text','Auteur',1,0,0,0,0,0,NULL,2),(46,6,'title','text','Titre',1,1,1,1,1,1,NULL,3),(47,6,'excerpt','text_area','Extrait',1,0,1,1,1,1,NULL,4),(48,6,'body','rich_text_box','Corps',1,0,1,1,1,1,NULL,5),(49,6,'slug','text','Slug',1,0,1,1,1,1,'{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}',6),(50,6,'meta_description','text','Meta Description',1,0,1,1,1,1,NULL,7),(51,6,'meta_keywords','text','Meta Mots-clés',1,0,1,1,1,1,NULL,8),(52,6,'status','select_dropdown','Statut',1,1,1,1,1,1,'{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}',9),(53,6,'created_at','timestamp','Créé le',1,1,1,0,0,0,NULL,10),(54,6,'updated_at','timestamp','Mis à jour le',1,0,0,0,0,0,NULL,11),(55,6,'image','image','Image de la page',0,1,1,1,1,1,NULL,12),(64,8,'id','text','Id',1,0,0,0,0,0,'{}',1),(65,8,'title','text','Title',1,1,1,1,1,1,'{}',3),(66,8,'content','text','Content',1,1,1,1,1,1,'{}',4),(67,8,'user_id','text','User Id',1,1,1,1,1,1,'{}',2),(68,8,'excerpt','text','Excerpt',1,1,1,1,1,1,'{}',5),(69,8,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',6),(70,8,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',7),(71,8,'image','image','Image',1,1,1,1,1,1,'{}',8),(106,14,'id','text','Id',1,0,0,0,0,0,'{}',1),(107,14,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',2),(108,14,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',3),(109,14,'Nom_scop','text','Nom Scop',1,1,1,1,1,1,'{}',4),(110,14,'Activites','text','Activites',0,1,1,1,1,1,'{}',5),(111,14,'Departement','text','Departement',0,1,1,1,1,1,'{}',6),(112,14,'Mail','text','Mail',0,1,1,1,1,1,'{}',7),(113,14,'Telephone','text','Telephone',0,1,1,1,1,1,'{}',8),(114,14,'Site_internet','text','Site Internet',0,1,1,1,1,1,'{}',9),(115,14,'Engagee_RSE','text','Engagee RSE',0,1,1,1,1,1,'{}',10),(116,14,'Labellisee_RSE','text','Labellisee RSE',0,1,1,1,1,1,'{}',11),(117,15,'id','text','Id',1,0,0,0,0,0,'{}',1),(118,15,'title','text','Title',1,1,1,1,1,1,'{}',2),(119,15,'content','text','Content',1,1,1,1,1,1,'{}',3),(120,15,'date_event','text','Date Event',1,1,1,1,1,1,'{}',4),(121,15,'place','text','Place',0,1,1,1,1,1,'{}',5),(122,15,'contact','text','Contact',0,1,1,1,1,1,'{}',6),(123,15,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',7),(124,15,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',8);
/*!40000 ALTER TABLE `data_rows` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_types`
--

DROP TABLE IF EXISTS `data_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_types`
--

LOCK TABLES `data_types` WRITE;
/*!40000 ALTER TABLE `data_types` DISABLE KEYS */;
INSERT INTO `data_types` VALUES (1,'users','users','Utilisateur','Utilisateurs','voyager-person','TCG\\Voyager\\Models\\User','TCG\\Voyager\\Policies\\UserPolicy','TCG\\Voyager\\Http\\Controllers\\VoyagerUserController','',1,0,NULL,'2019-12-04 16:18:00','2019-12-04 16:18:00'),(2,'menus','menus','Menu','Menus','voyager-list','TCG\\Voyager\\Models\\Menu',NULL,'','',1,0,NULL,'2019-12-04 16:18:00','2019-12-04 16:18:00'),(3,'roles','roles','Rôle','Rôles','voyager-lock','TCG\\Voyager\\Models\\Role',NULL,'','',1,0,NULL,'2019-12-04 16:18:00','2019-12-04 16:18:00'),(4,'categories','categories','Catégorie','Catégories','voyager-categories','TCG\\Voyager\\Models\\Category',NULL,'','',1,0,NULL,'2019-12-04 16:18:01','2019-12-04 16:18:01'),(5,'posts','posts','Post','Posts','voyager-news','TCG\\Voyager\\Models\\Post','TCG\\Voyager\\Policies\\PostPolicy','','',1,0,NULL,'2019-12-04 16:18:01','2019-12-04 16:18:01'),(6,'pages','pages','Page','Pages','voyager-file-text','TCG\\Voyager\\Models\\Page',NULL,'','',1,0,NULL,'2019-12-04 16:18:01','2019-12-04 16:18:01'),(8,'news','news','News','News',NULL,'App\\News',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}','2019-12-06 07:22:41','2019-12-06 09:33:52'),(14,'scops','scops','Scop','Scops',NULL,'App\\Scop',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}','2019-12-06 09:02:11','2019-12-06 09:02:11'),(15,'events','events','Event','Events',NULL,'App\\Event',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}','2019-12-06 09:22:23','2019-12-06 09:22:23');
/*!40000 ALTER TABLE `data_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_event` date NOT NULL,
  `place` text COLLATE utf8mb4_unicode_ci,
  `contact` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (2,'Atelier Club SCOP','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nullam feugiat, turpis at pulvinar vulputate, erat libero tristique tellus, nec bibendum odio risus sit amet ante. Aliquam erat volutpat. Nunc auctor. Mauris pretium quam et urna. Fusce nibh. Duis risus. Curabitur sagittis hendrerit','2020-05-22','Capitole','sudouest@scopbtp.org','2019-12-06 09:23:51','2019-12-06 09:23:51'),(3,'Assemblée Générale','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nullam feugiat, turpis at pulvinar vulputate, erat libero tristique tellus, nec bibendum odio risus sit amet ante. Aliquam erat volutpat. Nunc auctor. Mauris pretium quam et urna. Fusce nibh. Duis risus. Curabitur sagittis hendrerit','2020-06-30','Siège de la Fédération','sudouest@scopbtp.org','2019-12-06 09:24:30','2019-12-06 09:24:30'),(4,'Jour de Fête','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nullam feugiat, turpis at pulvinar vulputate, erat libero tristique tellus, nec bibendum odio risus sit amet ante. Aliquam erat volutpat. Nunc auctor. Mauris pretium quam et urna. Fusce nibh. Duis risus. Curabitur sagittis hendrerit','2020-06-22','Concorde','sudouest@scopbtp.org','2019-12-06 09:40:34','2019-12-06 09:40:34');
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_items`
--

DROP TABLE IF EXISTS `menu_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_items`
--

LOCK TABLES `menu_items` WRITE;
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
INSERT INTO `menu_items` VALUES (1,1,'Tableau de bord','','_self','voyager-boat',NULL,NULL,1,'2019-12-04 16:18:00','2019-12-04 16:18:00','voyager.dashboard',NULL),(2,1,'Médiathèque','','_self','voyager-images',NULL,NULL,5,'2019-12-04 16:18:00','2019-12-04 16:18:00','voyager.media.index',NULL),(3,1,'Utilisateurs','','_self','voyager-person',NULL,NULL,3,'2019-12-04 16:18:00','2019-12-04 16:18:00','voyager.users.index',NULL),(4,1,'Rôles','','_self','voyager-lock',NULL,NULL,2,'2019-12-04 16:18:00','2019-12-04 16:18:00','voyager.roles.index',NULL),(5,1,'Outils','','_self','voyager-tools',NULL,NULL,9,'2019-12-04 16:18:00','2019-12-04 16:18:00',NULL,NULL),(6,1,'Créateur de menus','','_self','voyager-list',NULL,5,10,'2019-12-04 16:18:00','2019-12-04 16:18:00','voyager.menus.index',NULL),(7,1,'Base de données','','_self','voyager-data',NULL,5,11,'2019-12-04 16:18:00','2019-12-04 16:18:00','voyager.database.index',NULL),(8,1,'voyager::seeders.menu_items.compass','','_self','voyager-compass',NULL,5,12,'2019-12-04 16:18:00','2019-12-04 16:18:00','voyager.compass.index',NULL),(9,1,'BREAD','','_self','voyager-bread',NULL,5,13,'2019-12-04 16:18:00','2019-12-04 16:18:00','voyager.bread.index',NULL),(10,1,'Paramètres','','_self','voyager-settings',NULL,NULL,14,'2019-12-04 16:18:00','2019-12-04 16:18:00','voyager.settings.index',NULL),(11,1,'Catégories','','_self','voyager-categories',NULL,NULL,8,'2019-12-04 16:18:01','2019-12-04 16:18:01','voyager.categories.index',NULL),(12,1,'Posts','','_self','voyager-news',NULL,NULL,6,'2019-12-04 16:18:01','2019-12-04 16:18:01','voyager.posts.index',NULL),(13,1,'Pages','','_self','voyager-file-text',NULL,NULL,7,'2019-12-04 16:18:01','2019-12-04 16:18:01','voyager.pages.index',NULL),(14,1,'Hooks','','_self','voyager-hook',NULL,NULL,13,'2019-12-04 16:18:02','2019-12-04 16:18:02','voyager.hooks',NULL),(16,1,'News','','_self',NULL,NULL,NULL,16,'2019-12-06 07:22:41','2019-12-06 07:22:41','voyager.news.index',NULL),(18,1,'Scops','','_self',NULL,NULL,NULL,18,'2019-12-06 09:02:11','2019-12-06 09:02:11','voyager.scops.index',NULL),(19,1,'Events','','_self',NULL,NULL,NULL,19,'2019-12-06 09:22:23','2019-12-06 09:22:23','voyager.events.index',NULL);
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,'admin','2019-12-04 16:18:00','2019-12-04 16:18:00');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_01_01_000000_add_voyager_user_fields',1),(4,'2016_01_01_000000_create_data_types_table',1),(5,'2016_01_01_000000_create_posts_table',1),(6,'2016_02_15_204651_create_categories_table',1),(7,'2016_05_19_173453_create_menu_table',1),(8,'2016_10_21_190000_create_roles_table',1),(9,'2016_10_21_190000_create_settings_table',1),(10,'2016_11_30_135954_create_permission_table',1),(11,'2016_11_30_141208_create_permission_role_table',1),(12,'2016_12_26_201236_data_types__add__server_side',1),(13,'2017_01_13_000000_add_route_to_menu_items_table',1),(14,'2017_01_14_005015_create_translations_table',1),(15,'2017_01_15_000000_make_table_name_nullable_in_permissions_table',1),(16,'2017_03_06_000000_add_controller_to_data_types_table',1),(17,'2017_04_11_000000_alter_post_nullable_fields_table',1),(18,'2017_04_21_000000_add_order_to_data_rows_table',1),(19,'2017_07_05_210000_add_policyname_to_data_types_table',1),(20,'2017_08_05_000000_add_group_to_settings_table',1),(21,'2017_11_26_013050_add_user_role_relationship',1),(22,'2017_11_26_015000_create_user_roles_table',1),(23,'2018_03_11_000000_add_user_settings',1),(24,'2018_03_14_000000_add_details_to_data_types_table',1),(25,'2018_03_16_000000_make_settings_value_nullable',1),(26,'2019_12_03_105658_create_news_table',1),(27,'2019_12_03_105659_add_foreign_keys_to_news_table',1),(28,'2019_12_03_123152_create_events_table',1),(29,'2019_12_04_104810_create_scops_table',1),(30,'2016_01_01_000000_create_pages_table',2),(31,'2019_12_06_084827_create_scops_table',0);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `excerpt` varchar(155) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `actualitys_user_id_foreign` (`user_id`),
  CONSTRAINT `actualitys_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (3,'Le retour du Jedi','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nullam feugiat, turpis at pulvinar vulputate, erat libero tristique tellus, nec bibendum odio risus sit amet ante. Aliquam erat volutpat. Nunc auctor. Mauris pretium quam et urna. Fusce nibh. Duis risus. Curabitur sagittis hendrerit',1,'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nullam feu','2019-12-06 09:35:19','2019-12-06 09:35:19','news/December2019/c3FsJgYSQybSRU8eSYds.png'),(4,'Groupe de travail n°6 du Club RSE','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nullam feugiat, turpis at pulvinar vulputate, erat libero tristique tellus, nec bibendum odio risus sit amet ante. Aliquam erat volutpat. Nunc auctor. Mauris pretium quam et urna. Fusce nibh. Duis risus. Curabitur sagittis hendrerit',1,'Lorem','2019-12-06 09:36:05','2019-12-06 09:36:05','news/December2019/raC8UIVNGGsMUXvjEqK7.jpg'),(5,'Nouveau site Internet','Lorem lkdshgqkdgh dsqkut hlsqkhtliuq ybkh gliqhdglquvy nqglkjdliqus mnqhtywkjhliud nldwhgli',1,'2019-12-06 10:35:19','2019-12-06 09:38:00','2019-12-06 09:38:59','news/December2019/aY6eQttl7OAQhJh0u8WJ.jpg');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pages_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,0,'Hello World','Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.','<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>','pages/page1.jpg','hello-world','Yar Meta Description','Keyword1, Keyword2','ACTIVE','2019-12-04 16:18:01','2019-12-04 16:18:01');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,1),(26,1),(27,1),(28,1),(29,1),(30,1),(31,1),(32,1),(33,1),(34,1),(35,1),(36,1),(37,1),(38,1),(39,1),(40,1),(47,1),(48,1),(49,1),(50,1),(51,1),(57,1),(58,1),(59,1),(60,1),(61,1),(62,1),(63,1),(64,1),(65,1),(66,1);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'browse_admin',NULL,'2019-12-04 16:18:00','2019-12-04 16:18:00'),(2,'browse_bread',NULL,'2019-12-04 16:18:00','2019-12-04 16:18:00'),(3,'browse_database',NULL,'2019-12-04 16:18:00','2019-12-04 16:18:00'),(4,'browse_media',NULL,'2019-12-04 16:18:00','2019-12-04 16:18:00'),(5,'browse_compass',NULL,'2019-12-04 16:18:00','2019-12-04 16:18:00'),(6,'browse_menus','menus','2019-12-04 16:18:00','2019-12-04 16:18:00'),(7,'read_menus','menus','2019-12-04 16:18:00','2019-12-04 16:18:00'),(8,'edit_menus','menus','2019-12-04 16:18:00','2019-12-04 16:18:00'),(9,'add_menus','menus','2019-12-04 16:18:00','2019-12-04 16:18:00'),(10,'delete_menus','menus','2019-12-04 16:18:00','2019-12-04 16:18:00'),(11,'browse_roles','roles','2019-12-04 16:18:00','2019-12-04 16:18:00'),(12,'read_roles','roles','2019-12-04 16:18:00','2019-12-04 16:18:00'),(13,'edit_roles','roles','2019-12-04 16:18:00','2019-12-04 16:18:00'),(14,'add_roles','roles','2019-12-04 16:18:00','2019-12-04 16:18:00'),(15,'delete_roles','roles','2019-12-04 16:18:00','2019-12-04 16:18:00'),(16,'browse_users','users','2019-12-04 16:18:00','2019-12-04 16:18:00'),(17,'read_users','users','2019-12-04 16:18:00','2019-12-04 16:18:00'),(18,'edit_users','users','2019-12-04 16:18:00','2019-12-04 16:18:00'),(19,'add_users','users','2019-12-04 16:18:00','2019-12-04 16:18:00'),(20,'delete_users','users','2019-12-04 16:18:00','2019-12-04 16:18:00'),(21,'browse_settings','settings','2019-12-04 16:18:00','2019-12-04 16:18:00'),(22,'read_settings','settings','2019-12-04 16:18:00','2019-12-04 16:18:00'),(23,'edit_settings','settings','2019-12-04 16:18:01','2019-12-04 16:18:01'),(24,'add_settings','settings','2019-12-04 16:18:01','2019-12-04 16:18:01'),(25,'delete_settings','settings','2019-12-04 16:18:01','2019-12-04 16:18:01'),(26,'browse_categories','categories','2019-12-04 16:18:01','2019-12-04 16:18:01'),(27,'read_categories','categories','2019-12-04 16:18:01','2019-12-04 16:18:01'),(28,'edit_categories','categories','2019-12-04 16:18:01','2019-12-04 16:18:01'),(29,'add_categories','categories','2019-12-04 16:18:01','2019-12-04 16:18:01'),(30,'delete_categories','categories','2019-12-04 16:18:01','2019-12-04 16:18:01'),(31,'browse_posts','posts','2019-12-04 16:18:01','2019-12-04 16:18:01'),(32,'read_posts','posts','2019-12-04 16:18:01','2019-12-04 16:18:01'),(33,'edit_posts','posts','2019-12-04 16:18:01','2019-12-04 16:18:01'),(34,'add_posts','posts','2019-12-04 16:18:01','2019-12-04 16:18:01'),(35,'delete_posts','posts','2019-12-04 16:18:01','2019-12-04 16:18:01'),(36,'browse_pages','pages','2019-12-04 16:18:01','2019-12-04 16:18:01'),(37,'read_pages','pages','2019-12-04 16:18:01','2019-12-04 16:18:01'),(38,'edit_pages','pages','2019-12-04 16:18:01','2019-12-04 16:18:01'),(39,'add_pages','pages','2019-12-04 16:18:01','2019-12-04 16:18:01'),(40,'delete_pages','pages','2019-12-04 16:18:01','2019-12-04 16:18:01'),(41,'browse_hooks',NULL,'2019-12-04 16:18:02','2019-12-04 16:18:02'),(47,'browse_news','news','2019-12-06 07:22:41','2019-12-06 07:22:41'),(48,'read_news','news','2019-12-06 07:22:41','2019-12-06 07:22:41'),(49,'edit_news','news','2019-12-06 07:22:41','2019-12-06 07:22:41'),(50,'add_news','news','2019-12-06 07:22:41','2019-12-06 07:22:41'),(51,'delete_news','news','2019-12-06 07:22:41','2019-12-06 07:22:41'),(57,'browse_scops','scops','2019-12-06 09:02:11','2019-12-06 09:02:11'),(58,'read_scops','scops','2019-12-06 09:02:11','2019-12-06 09:02:11'),(59,'edit_scops','scops','2019-12-06 09:02:11','2019-12-06 09:02:11'),(60,'add_scops','scops','2019-12-06 09:02:11','2019-12-06 09:02:11'),(61,'delete_scops','scops','2019-12-06 09:02:11','2019-12-06 09:02:11'),(62,'browse_events','events','2019-12-06 09:22:23','2019-12-06 09:22:23'),(63,'read_events','events','2019-12-06 09:22:23','2019-12-06 09:22:23'),(64,'edit_events','events','2019-12-06 09:22:23','2019-12-06 09:22:23'),(65,'add_events','events','2019-12-06 09:22:23','2019-12-06 09:22:23'),(66,'delete_events','events','2019-12-06 09:22:23','2019-12-06 09:22:23');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `posts_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,0,NULL,'Lorem Ipsum Post',NULL,'This is the excerpt for the Lorem Ipsum Post','<p>This is the body of the lorem ipsum post</p>','posts/post1.jpg','lorem-ipsum-post','This is the meta description','keyword1, keyword2, keyword3','PUBLISHED',0,'2019-12-04 16:18:01','2019-12-04 16:18:01'),(2,0,NULL,'My Sample Post',NULL,'This is the excerpt for the sample Post','<p>This is the body for the sample post, which includes the body.</p>\n                <h2>We can use all kinds of format!</h2>\n                <p>And include a bunch of other stuff.</p>','posts/post2.jpg','my-sample-post','Meta Description for sample post','keyword1, keyword2, keyword3','PUBLISHED',0,'2019-12-04 16:18:01','2019-12-04 16:18:01'),(3,0,NULL,'Latest Post',NULL,'This is the excerpt for the latest post','<p>This is the body for the latest post</p>','posts/post3.jpg','latest-post','This is the meta description','keyword1, keyword2, keyword3','PUBLISHED',0,'2019-12-04 16:18:01','2019-12-04 16:18:01'),(4,0,NULL,'Yarr Post',NULL,'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.','<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>','posts/post4.jpg','yarr-post','this be a meta descript','keyword1, keyword2, keyword3','PUBLISHED',0,'2019-12-04 16:18:01','2019-12-04 16:18:01');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin','Administrator','2019-12-04 16:13:43','2019-12-04 16:13:43'),(2,'user','Utilisateur standard','2019-12-04 16:18:00','2019-12-04 16:18:00');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scops`
--

DROP TABLE IF EXISTS `scops`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scops` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `Nom_scop` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Activites` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Departement` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Mail` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Telephone` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Site_internet` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Engagee_RSE` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Labellisee_RSE` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scops`
--

LOCK TABLES `scops` WRITE;
/*!40000 ALTER TABLE `scops` DISABLE KEYS */;
INSERT INTO `scops` VALUES (1,'2019-12-06 09:03:35','2019-12-06 09:03:35','AHJ',NULL,'81','mail@mail.fr','00 00 00 00 00','www.ahj-tarn.fr','oui','non'),(2,'2019-12-06 09:06:30','2019-12-06 09:06:30','Cabrol','Ossature métallique, Bardage / Vêture...','81',NULL,'05 63 61 38 71','http://cabrol.fr/','oui','non'),(3,'2019-12-06 09:07:41','2019-12-06 09:07:41','BV SCOP',NULL,'09','bvscop@bvscop.fr','05.34.01.31.00','http://www.bvscop.com/','oui','non'),(4,'2019-12-06 09:09:33','2019-12-06 09:09:33','La tournée du Coq',NULL,'31','contact@latourneeducoq.com','05 34 51 38 31','https://latourneeducoq.com/','oui','non'),(5,'2019-12-06 09:14:00','2019-12-06 09:14:00','Couserans Construction','Maçonnerie, Couverture','09','contact@couseransconstruction.fr','05.61.66.27.62','http://www.couseransconstruction.fr/','oui','oui'),(6,'2019-12-06 09:16:46','2019-12-06 09:16:46','Regate Regabat','coopérative généraliste','81','info@caetarn.fr','05 63 62 82 84','https://www.regate.fr/','oui','oui');
/*!40000 ALTER TABLE `scops` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'site.title','Title du site','Title du site','','text',1,'Site'),(2,'site.description','Description du site','Description du site','','text',2,'Site'),(3,'site.logo','Logo du site','','','image',3,'Site'),(4,'site.google_analytics_tracking_id','Google Analytics ID de Tracking','','','text',4,'Site'),(5,'admin.bg_image','Image de fond de l\'espace admin','','','image',5,'Admin'),(6,'admin.title','Titre de l\'espace admin','Voyager','','text',1,'Admin'),(7,'admin.description','Description de l\'espace admin','Bienvenue dans Voyager, le panneau d\'administration qui manquait à Laravel.','','text',2,'Admin'),(8,'admin.loader','Chargement de l\'espace admin','','','image',3,'Admin'),(9,'admin.icon_image','Icône de l\'espace admin','','','image',4,'Admin'),(10,'admin.google_analytics_client_id','Google Analytics ID Client (Utilisé pour le panneau d\'administration)','','','text',1,'Admin');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translations`
--

DROP TABLE IF EXISTS `translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translations`
--

LOCK TABLES `translations` WRITE;
/*!40000 ALTER TABLE `translations` DISABLE KEYS */;
INSERT INTO `translations` VALUES (1,'data_types','display_name_singular',5,'pt','Post','2019-12-04 16:18:01','2019-12-04 16:18:01'),(2,'data_types','display_name_singular',6,'pt','Página','2019-12-04 16:18:01','2019-12-04 16:18:01'),(3,'data_types','display_name_singular',1,'pt','Utilizador','2019-12-04 16:18:01','2019-12-04 16:18:01'),(4,'data_types','display_name_singular',4,'pt','Categoria','2019-12-04 16:18:01','2019-12-04 16:18:01'),(5,'data_types','display_name_singular',2,'pt','Menu','2019-12-04 16:18:01','2019-12-04 16:18:01'),(6,'data_types','display_name_singular',3,'pt','Função','2019-12-04 16:18:01','2019-12-04 16:18:01'),(7,'data_types','display_name_plural',5,'pt','Posts','2019-12-04 16:18:01','2019-12-04 16:18:01'),(8,'data_types','display_name_plural',6,'pt','Páginas','2019-12-04 16:18:01','2019-12-04 16:18:01'),(9,'data_types','display_name_plural',1,'pt','Utilizadores','2019-12-04 16:18:01','2019-12-04 16:18:01'),(10,'data_types','display_name_plural',4,'pt','Categorias','2019-12-04 16:18:01','2019-12-04 16:18:01'),(11,'data_types','display_name_plural',2,'pt','Menus','2019-12-04 16:18:01','2019-12-04 16:18:01'),(12,'data_types','display_name_plural',3,'pt','Funções','2019-12-04 16:18:01','2019-12-04 16:18:01'),(13,'categories','slug',1,'pt','categoria-1','2019-12-04 16:18:01','2019-12-04 16:18:01'),(14,'categories','name',1,'pt','Categoria 1','2019-12-04 16:18:01','2019-12-04 16:18:01'),(15,'categories','slug',2,'pt','categoria-2','2019-12-04 16:18:01','2019-12-04 16:18:01'),(16,'categories','name',2,'pt','Categoria 2','2019-12-04 16:18:01','2019-12-04 16:18:01'),(17,'pages','title',1,'pt','Olá Mundo','2019-12-04 16:18:01','2019-12-04 16:18:01'),(18,'pages','slug',1,'pt','ola-mundo','2019-12-04 16:18:01','2019-12-04 16:18:01'),(19,'pages','body',1,'pt','<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>','2019-12-04 16:18:01','2019-12-04 16:18:01'),(20,'menu_items','title',1,'pt','Painel de Controle','2019-12-04 16:18:01','2019-12-04 16:18:01'),(21,'menu_items','title',2,'pt','Media','2019-12-04 16:18:01','2019-12-04 16:18:01'),(22,'menu_items','title',12,'pt','Publicações','2019-12-04 16:18:01','2019-12-04 16:18:01'),(23,'menu_items','title',3,'pt','Utilizadores','2019-12-04 16:18:01','2019-12-04 16:18:01'),(24,'menu_items','title',11,'pt','Categorias','2019-12-04 16:18:01','2019-12-04 16:18:01'),(25,'menu_items','title',13,'pt','Páginas','2019-12-04 16:18:01','2019-12-04 16:18:01'),(26,'menu_items','title',4,'pt','Funções','2019-12-04 16:18:01','2019-12-04 16:18:01'),(27,'menu_items','title',5,'pt','Ferramentas','2019-12-04 16:18:01','2019-12-04 16:18:01'),(28,'menu_items','title',6,'pt','Menus','2019-12-04 16:18:01','2019-12-04 16:18:01'),(29,'menu_items','title',7,'pt','Base de dados','2019-12-04 16:18:01','2019-12-04 16:18:01'),(30,'menu_items','title',10,'pt','Configurações','2019-12-04 16:18:01','2019-12-04 16:18:01');
/*!40000 ALTER TABLE `translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `user_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `user_roles_user_id_index` (`user_id`),
  KEY `user_roles_role_id_index` (`role_id`),
  CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'Cécile','cec.couderc@gmail.com','users/default.png',NULL,'$2y$10$2G/lwoeZ/lhTKDZBZRc8EOLY26BwtgAGEfemokqS3u5vAL1d5Aifu',NULL,NULL,'2019-12-04 16:13:43','2019-12-04 16:13:43');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-06 11:45:18
